﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    private Animator playerAnimator;
    public float walkingSpeed = 1.5f;
    public float speed;
    public float rotationSpeed = 100.0f;
    private float faster;
    public float moreFaster = 0f;

    void Start()
    {
        playerAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        float movement = Input.GetAxis("Vertical") * walkingSpeed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        movement *= Time.deltaTime;
        rotation *= Time.deltaTime;
        speed *= Time.deltaTime;
        float move = movement + speed;
        transform.Translate(0, 0, move);
        transform.Rotate(0, rotation, 0);

        if (Input.GetKey("w"))
        {
            playerAnimator.SetBool("isWalking", true);
        }
        else
        {
            playerAnimator.SetBool("isWalking", false);
        }
        if (Input.GetKey("s"))
        {
            playerAnimator.SetBool("isWalkingBackward", true);
        }
        else
        {
            playerAnimator.SetBool("isWalkingBackward", false);
        }

        if (Input.GetKey("left shift"))
        {
            faster = 4f + moreFaster;
            speed = faster;
            playerAnimator.SetBool("isRunning", true);
        }
        else
        {
            speed = 0f;
            playerAnimator.SetBool("isRunning", false);
        }
        if(Input.GetKeyDown("space") && Input.GetKey("w"))
        {
            playerAnimator.SetBool("isJumping", true);
            faster = 3f + moreFaster;
            speed = faster;
        }
        else if(Input.GetKeyDown("space"))
        {
            playerAnimator.SetBool("isJumpFromIdle", true);
            speed = 0;
            movement = 0;
        }
        else
        {
            playerAnimator.SetBool("isJumping", false);
            playerAnimator.SetBool("isJumpFromIdle", false);
        }
    }
}
