﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public Transform target;
    public GameObject player;
    public float horizontalSpeed = 2.0f;
    public float verticalSpeed = 2.0f;

    private float hSpeed = 0.0f;
    private float vSpeed = 0.0f;
    private Vector3 offset;

    void Start()
    {
        offset = transform.position - player.transform.position;
    }
    void Update()
    {
        target.LookAt(target);
        hSpeed += horizontalSpeed * Input.GetAxis("Mouse X");
        vSpeed -= verticalSpeed * Input.GetAxis("Mouse Y");
        transform.eulerAngles = new Vector3(vSpeed, hSpeed, 0.0f);
    }

    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
}
